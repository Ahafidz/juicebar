/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juicebar;

/**
 *
 * @author Rahman
 */
public class Kira2 {
    
    double max = 310;
    double fruita, fruitb, fruitc, pricea, priceb, pricec;
    double resulta = 0, resultb = 0, resultc = 0, totprice = 0, totcal = 0;
    public Kira2() {
    }
    
                 //max ml fruit a,b,c       //price per ml                         
    public Kira2(double a, double b, double c, double pa, double pb, double pc ) {
        fruita = a;
        fruitb = b;
        fruitc = c;
        pricea = pa;
        priceb = pb;
        pricec = pc;
    }
    
    public void calcRatio(){
       
        boolean check = true;
        if( fruita < max && fruitb < max && fruitc < max){
            calcNoMax();         
        }         
        else if(fruita == max && fruitb == max && fruitc == max){
            calcAllMax();                
        }
        else if(fruita == max && fruitb == max && fruitc < max)
        {
            calcFruitAandBmax();
        }
        else if(fruita == max && fruitc == max && fruitb < max)
        {
            calcFruitAandCmax();    
        }
        else if(fruitb == max && fruitc == max && fruita < max)
        {
            calcFruitBandCmax();
        }
        else if (fruita == max && fruitb < max && fruitc < max){
            calcFruitAmax();
        }
        else if (fruitb == max && fruita < max && fruitc < max){
            calcFruitBmax();
        }
        else if (fruitc == max && fruita < max && fruitb < max){
            calcFruitCmax();
        }
        else
        {
            System.out.println("Your fruit cannot be more than 310ml");
            check = false;
        }
        if(check == true)
        {
            calcPrice();
            printFruits();
        }
    }
    
    public void calcNoMax(){
        double tot = fruita + fruitb + fruitc;
        System.out.println("Total: " +tot);
        double rem = max - tot;
        System.out.println("Remaining: " +rem);

        double pera = fruita/tot * 100;
        double perb = fruitb/tot * 100;
        double perc = fruitc/tot * 100;

        resulta = (pera/100 * rem) + fruita;
        resultb = (perb/100 * rem) + fruitb;
        resultc = (perc/100 * rem) + fruitc;
    }
    
    public void calcAllMax(){
        resulta = fruita/3;
        resultb = fruitb/3;
        resultc = fruitc/3;
    }
    
    public void calcFruitAandBmax(){
        double rem = max - fruitc;
        resulta = rem / 2;
        resultb = rem / 2;
        resultc = fruitc;
    }
    
    public void calcFruitAandCmax(){
        double rem = max - fruitb;
        resulta = rem / 2;
        resultb = fruitb;
        resultc = rem / 2;
    }

    public void calcFruitBandCmax(){
        double rem = max - fruita;
        resulta = fruita;
        resultb = rem / 2;
        resultc = rem / 2;
    }
    
    public void calcFruitAmax(){
        System.out.println("Fruita:" +fruita);
        System.out.println("Fruitb:" +fruitb);
        System.out.println("Fruitc:" +fruitc);
        resulta = max - (fruitb + fruitc);
        resultb = fruitb;
        resultc = fruitc;
    }
    
    public void calcFruitBmax(){
        System.out.println("Fruita:" +fruita);
        System.out.println("Fruitb:" +fruitb);
        System.out.println("Fruitc:" +fruitc);
        resulta = fruita;
        resultb = max - (fruita + fruitc);
        resultc = fruitc;
    }
    
    public void calcFruitCmax(){
        resulta = fruita;
        resultb = fruitb;
        resultc = max - (fruita + fruitb); 
    }
    
    public void calcPrice(){
        pricea = resulta * pricea;
        priceb = resultb * priceb;
        pricec = resultc * pricec;
        totprice = pricea + priceb + pricec;
        System.out.println("Real Price (RM): "+totprice);        
        if(totprice <= 10.90)
            totprice = 10.90;
        else if(totprice <= 11.9)
            totprice = 11.9;
        else if(totprice <= 12.9)
            totprice = 12.9;
        else if(totprice <= 13.9)
            totprice = 13.9;
        else if(totprice <= 14.9)
            totprice = 14.9;
        else
            totprice = 15.9;
            
        
     }
    
    
    public double calcNutri(double a,double b,double c){
        double totalN;
        double nuta = (a/100) * resulta;
        double nutb = (b/100) * resultb;
        double nutc = (c/100) * resultc;
        totalN = nuta + nutb + nutc;
    return totalN;
}

    public double getFruita() {
        return fruita;
    }

    public double getFruitb() {
        return fruitb;
    }

    public double getFruitc() {
        return fruitc;
    }

    public double getResulta() {
        return resulta;
    }

    public double getResultb() {
        return resultb;
    }

    public double getResultc() {
        return resultc;
    }

    public double getTotprice() {
        return totprice;
    }
    public void printFruits()
    {
        System.out.println("fruit a (ml):" + String.format("%.2f", resulta));
        System.out.println("fruit b (ml):" + String.format("%.2f", resultb));
        System.out.println("fruit c (ml):" + String.format("%.2f", resultc));
        System.out.println("Total fruit (ml): "+String.format("%.2f", (resulta+resultb+resultc)));
        System.out.println("Price a (RM):" + String.format("%.2f", pricea));
        System.out.println("Price b (RM):" + String.format("%.2f", priceb));
        System.out.println("Price c (RM):" + String.format("%.2f", pricec));
        System.out.println("Total Price (RM): "+String.format("%.2f", (totprice)));
        System.out.println("Total Calories (cal): "+String.format("%.2f", (totcal)));
    }
    
    public static void main(String args[]) {
        Kira2 cal = new Kira2(40,310,0,00.1,0.2,0);
        cal.calcRatio();
    }
}
