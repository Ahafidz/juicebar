/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juicebar;

/**
 *
 * @author User
 */
public class Fruit {
    
    int id;
    String name;
    double vitaminb;
    double vitamina;
    double vitaminc;
    double vitamine;
    double cal;
    String effect;
    double price;
    int max;

    public Fruit(int id, String name, double vitaminb, double vitamina, double vitaminc, 
            double vitamine, double cal, String effect, double price, int max) {
        this.id = id;
        this.name = name;
        this.vitaminb = vitaminb;
        this.vitamina = vitamina;
        this.vitaminc = vitaminc;
        this.vitamine = vitamine;
        this.cal = cal;
        this.effect = effect;
        this.price = price;
        this.max = max;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getMax() {
        return max;
    }
    
    
    
    
}
