/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juicebar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author User
 */
public class ConnectionManager {
    private static String url = "jdbc:derby://localhost:1527/JuiceBar";    
    private static String username = "user1";   
    private static String password = "user1";
    private static Connection con;

    public static Connection getConnection() {
       try {
                con = DriverManager.getConnection(url, username, password);
            } catch (SQLException ex) {
                // log an exception. fro example:
                System.out.println("Failed to create the database connection. Error Code: "+ ex.getErrorCode()); 
            }
        return con;
    }
}
