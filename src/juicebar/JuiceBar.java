/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juicebar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class JuiceBar {

    /**
     * @param args the command line arguments
*/
    private static Connection con = null;
    private static Statement stmt = null;
    private static ResultSet rs = null;
   
    public static void main(String[] args) {
        // TODO code application logic here
        
        //testDBConn();
        
        //test (get ArrayList of Fruit obj)
//        ArrayList<Fruit> al = FruitsFunc.getFruitsList(new int[]{4,6,2});
//        for (Fruit f : al){
//            System.out.println(f.name);
//        }
        
        //array of max ml from array of id
//        int [] maxList = FruitsFunc.getMaxList(new int[]{4,0,0});
//        System.out.print("max list : ");
//        for(int max: maxList){
//            System.out.print(max+"; ");
//        }
        
        //array of price/ml from array of id
//        double[] priceList = FruitsFunc.getPriceList(new int[]{4,0,0});
//        System.out.print("Price list : ");
//        for(double p: priceList){
//            System.out.print(p+"; ");
//        }

        // test witout gui
        testDoublePortion();
        
        
    }

    private static void testDBConn() {
        try{
            con = ConnectionManager.getConnection();
            stmt = con.createStatement();
            String sql = "SELECT * FROM FRUITS";
            rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                int id_col = rs.getInt("ID");
                String nameFruit = rs.getString("Fruit");
                String maxml = rs.getString("maxml");
                String cal = rs.getString("cal");
                System.out.println(id_col+" "+nameFruit+" "+maxml+" "+cal);
            }
            
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
    }

    private static void testDoublePortion() {
        ArrayList<Fruit> fruitList = FruitsFunc.getFruitsList();
        ArrayList idFruit = new ArrayList();
        ArrayList idNoRedundant = new ArrayList();
        
        int[] maxs = new int[]{0,0,0};
        double[] prices = new double[]{0,0,0};
        double[] calories = new double[]{0,0,0};
        double[] vitaminA = new double[]{0,0,0};
        double[] vitaminB = new double[]{0,0,0};
        double[] vitaminC = new double[]{0,0,0};
        double[] vitaminE = new double[]{0,0,0};
        int idfDisplay = 1;
        for(Fruit f: fruitList){
            System.out.println(idfDisplay+") "+f.name);
            idfDisplay++;
        }
        
        Scanner sc = new Scanner(System.in);
        System.out.print("\nEnter 1st number : ");
        idFruit.add(sc.nextInt());
        System.out.print("\nEnter 2nd number : ");
        idFruit.add(sc.nextInt());
        System.out.print("\nEnter 3rd number : ");
        idFruit.add(sc.nextInt());
        
        for(int id = idFruit.size()-1; id>=0; id--){
            if ((int)idFruit.get(id)==0){
                idFruit.remove(id);
            }
        }
        
        
        for(int i=0;i<idFruit.size();i++){
            Fruit f = fruitList.get((int) idFruit.get(i)-1);
            maxs[i]= f.max;
            prices[i]= f.price;
            calories[i]= f.cal;
            vitaminA[i]= f.vitamina;
            vitaminB[i]= f.vitaminb;
            vitaminC[i]= f.vitaminc;
            vitaminE[i]= f.vitamine;
        }
        
        Kira2 cal = new Kira2(maxs[0],maxs[1],maxs[2],prices[0],prices[1],prices[2]);
        cal.calcRatio();
        double totalPrice = cal.getTotprice();
        System.out.println("\n================= Kira2.java ended ===================\n");
        
        System.out.println("RM " + String.format("%.2f", totalPrice));
        System.out.println("Calories : " + String.format("%.2f", cal.calcNutri(calories[0], calories[1], calories[2]))+" cal");
        System.out.println("Vitamin A: " + String.format("%.2f", cal.calcNutri(vitaminA[0], vitaminA[1], vitaminA[2]))+" mg");
        System.out.println("Vitamin B: " + String.format("%.2f", cal.calcNutri(vitaminB[0], vitaminB[1], vitaminB[2]))+" mg");
        System.out.println("Vitamin C: " + String.format("%.2f", cal.calcNutri(vitaminC[0], vitaminC[1], vitaminC[2]))+" mg");
        System.out.println("Vitamin E: " + String.format("%.2f", cal.calcNutri(vitaminE[0], vitaminE[1], vitaminE[2]))+" mg");
        
        idNoRedundant = removeRedundant(idFruit);
        System.out.println("Benefits: ");  
        System.out.println("idNoRedundant : "+idNoRedundant);
        
        for (int i=0; i<idNoRedundant.size();i++){
                System.out.println(" - "+fruitList.get((int) idNoRedundant.get(i)-1).effect);
        }
        System.out.println(fruitList.get((int) idFruit.get(0)-1).name+" : "+String.format("%.0f", cal.getResulta()) + " ml");
        if(idFruit.size() >= 2)
            System.out.println(fruitList.get((int) idFruit.get(1)-1).name+" : "+String.format("%.0f", cal.getResultb()) + " ml");
        if(idFruit.size() >= 3)
            System.out.println(fruitList.get((int) idFruit.get(2)-1).name+" : "+String.format("%.0f", cal.getResultc()) + " ml");
    }
    

    private static ArrayList removeRedundant(ArrayList idF) {
        ArrayList idList = new ArrayList();
        idList.add(idF.get(0));
        for(int i=0; i<idF.size();i++){
            boolean found = false;
            for(int j=0; j<idList.size();j++){
                if((int) idList.get(j) == (int) idF.get(i)){
                    found = true;
                }
            }
            if (!found){
                idList.add((int) idF.get(i));
            }
        }
        
        return idList;
    }
    
    
    
}
