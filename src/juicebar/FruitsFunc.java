/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juicebar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author User
    String s = "ID, FRUIT, PROTEIN, CALCIUM, MAGNESIUM, VITAMINB,  VITAMINA, VITAMINC, VITAMINE, CAL, EFFECT, PRICE, MAXML, GRAM";
 */
public class FruitsFunc {
    
    public static ArrayList getFruitsList(){
        ArrayList<Fruit> fruitsList = new ArrayList();
        String sql = "SELECT * FROM FRUITS";
        //System.out.println(sql);
        
        
        try{
            Connection con = ConnectionManager.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                fruitsList.add(new Fruit(rs.getInt("ID"), rs.getString("FRUIT"),
                        rs.getDouble("VITAMINB"), rs.getDouble("VITAMINA"), 
                        rs.getDouble("VITAMINC"), rs.getDouble("VITAMINE"), 
                        rs.getDouble("CAL"), rs.getString("EFFECT"), 
                        rs.getDouble("PRICE"),rs.getInt("MAXML")));
            }
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
        
        
        return fruitsList;
    }
    public static Fruit getFruitObj(int fruitsID){
        Fruit fruitsObj = null;
        String sql = "SELECT * FROM FRUITS WHERE ";
        sql += " ID = "+fruitsID;
        //System.out.println(sql);
        
        
        try{
            Connection con = ConnectionManager.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                fruitsObj = new Fruit(rs.getInt("ID"), rs.getString("FRUIT"),
                        rs.getDouble("VITAMINB"), rs.getDouble("VITAMINA"), 
                        rs.getDouble("VITAMINC"), rs.getDouble("VITAMINE"), 
                        rs.getDouble("CAL"), rs.getString("EFFECT"), 
                        rs.getDouble("PRICE"),rs.getInt("MAXML"));
            }
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
        
        
        return fruitsObj;
    }
    
    public static ArrayList getFruitsList(int[] fruitsID){
        ArrayList<Fruit> fruitsList = new ArrayList();
        String sql = "SELECT * FROM FRUITS WHERE ";
        for (int id : fruitsID){
            sql += " ID = "+id+" OR";
        }
        sql += "ending";
        sql = sql.replaceFirst(" ORending", " ");
        //System.out.println(sql);
        
        
        try{
            Connection con = ConnectionManager.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                fruitsList.add(new Fruit(rs.getInt("ID"), rs.getString("FRUIT"),
                        rs.getDouble("VITAMINB"), rs.getDouble("VITAMINA"), 
                        rs.getDouble("VITAMINC"), rs.getDouble("VITAMINE"), 
                        rs.getDouble("CAL"), rs.getString("EFFECT"), 
                        rs.getDouble("PRICE"),rs.getInt("MAXML")));
            }
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
        
        
        return fruitsList;
    }
    
    public static int[] getMaxList(int[] fruitsID){
        
        int[] maxList = new int[]{0,0,0};
        String sql = "SELECT MAXML FROM FRUITS WHERE ";
        for (int i = 0;i<3 && i<fruitsID.length;i++){
            sql += " ID = "+fruitsID[i]+" OR";
        }
        sql += "ending";
        sql = sql.replace(" ORending", " ");
        //System.out.println(sql);
        
        
        try{
            Connection con = ConnectionManager.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            int i = 0;
            while(rs.next()){
                maxList[i]=rs.getInt("MAXML");
                i++;
            }
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
        
        
        return maxList;
    }
    public static double[] getPriceList(int[] fruitsID){
        
        double[] priceList = new double[]{0,0,0};
        String sql = "SELECT PRICE FROM FRUITS WHERE ";
        for (int i = 0;i<3 && i<fruitsID.length;i++){
            sql += " ID = "+fruitsID[i]+" OR";
        }
        sql += "ending";
        sql = sql.replace(" ORending", " ");
        //System.out.println(sql);
        
        
        try{
            Connection con = ConnectionManager.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            int i = 0;
            while(rs.next()){
                priceList[i] = rs.getDouble("PRICE");
                i++;
            }
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
        
        
        return priceList;
    }
    
    public static boolean checkTotalMax(int[] fruitsID){
        int[] maxList = new int[]{0,0,0};
        int totalMax = 0;
        String sql = "SELECT MAXML FROM FRUITS WHERE ";
        for (int i = 0;i<3 && i<fruitsID.length;i++){
            sql += " ID = "+fruitsID[i]+" OR";
        }
        sql += "ending";
        sql = sql.replace(" ORending", " ");
        
        
        try{
            Connection con = ConnectionManager.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            int i = 0;
            while(rs.next()){
                maxList[i]=rs.getInt("MAXML");
                i++;
            }
            con.close();
        }catch(SQLException  err){
            System.out.println(err.getMessage());
        }
        for(int m:maxList){
            totalMax += m;
        }
        
        if (totalMax >= 310){
            return true;
        }
        return false;
    }
    
}
